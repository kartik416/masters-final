import spacy
from bs4 import BeautifulSoup
import requests
import pandas as pd
import regex as re
import gender_guesser.detector as gender
import json
from datetime import datetime
import numpy as np

#Loading Spacy Model
nlp = spacy.load("en_core_web_trf")

class Web_Scrapper:
    def __init__(self, URL:str):
        self.accuracy = True
        self.data_dict = {}
        self.metadata_dict = {}
        self.company_html_dict = {}
        self.URL = URL
        self.tracker = ""
        self.debug = False
        
    def name_extractor(self, sentence):
        #Data cleaning using regex
        sentence = re.sub(r"(\w)([A-Z])", r"\1 \2", sentence)
        sentence = sentence.replace('\n', ' ').replace('\t',' ')
        sentence = re.sub("[\(\[].*?[\)\]]", "", sentence)
        sentence = re.sub(' +', ' ', sentence)
        #print(sentence)
        doc = nlp(sentence) #passing sentence through spacy's nlp pipeline
        response = {"Status":"Fail"}
        for ent in doc.ents:
            #print(ent.text, ent.start_char, ent.end_char, ent.label_)
            if ent.label_ == "PERSON": #If any entity is labeled as person then get that entity
                if "our" not in ent.text.lower() or "story" not in ent.text.lower():
                    #print(ent.text)
                    response["Status"] = "Success"
                    try:
                        #print("it is a person")
                        name = str(ent.text).split(" ",1)
                        response["First_Name"] = name[0]
                        response["Last_Name"] = name[1]
                        #print("Try Response")
                        #print(extracted_data)
                    except:
                        response["First_Name"] = ent.text
                        response["Last_Name"] = "NA"
                        #print("Except Response")
        return response
    
    def sentence_creater(self, text):
        sentences = text.split(".")
        for sentence in sentences:
            chunk_size = 511 #If chunk is greater than 511 then split it more to avoid error in NLP model
            if len(sentence)>chunk_size:
                #print("inside if")
                result=[]
                for i in range(0,len(sentence),chunk_size):
                    temp = sentence[i:i+chunk_size]
                    temp_result = self.name_extractor(temp)
                    if temp_result["Status"] == "Success":
                        result.append(temp_result)
                    if len(result)>0:
                        return result
            else:
                result = self.name_extractor(sentence)
            return result
    
    
    def reverse_search_revised(self, name_text, soup):
        #print(name_text)
        response={
            "Status":"Fail",
            "data":{
                    "First_Name":[],
                    "Last_Name":[]   
            }
            }
        name_text = name_text.strip(" ") #Strip trailing spaces
        try:
            test_list = []
            possible_tags=["span","div","p","h1","h2","h3","h4","h5","h6","a","pre"] #list of tags that contains name data most of the time
            #Searching for tag whose text starts with the name provided
            for tag in possible_tags:
                for elements in soup.find_all(tag):
                    if str(name_text).lower() in elements.text.lower():
                        if len(elements.contents)<2 and ("<" not in str(elements.contents)):
                            if (elements.text.lower().strip()).startswith(str(name_text).lower()):
                                if elements.attrs!={}: #Check if elements have attributes
                                    test_list.append(elements) #add that element if it have attributes
            #In case there are no attributes then execute this codeblock
            if len(test_list)==0:
                for tag in possible_tags:
                    for elements in soup.find_all(tag):
                        if str(name_text).lower() in elements.text.lower():
                            if len(elements.contents)<2 and ("<" not in str(elements.contents)):
                                if (elements.text.lower().strip()).startswith(str(name_text).lower()):
                                    test_list.append(elements)
            
            #len(test_list)
            selected_t=""                                
            for t in test_list:
                #check if that tag have class
                if "class" in t.attrs:
                    tag_name, tag_attribute = t.name, {"class":t.attrs["class"]} #If it have class then set attribute as class
                else:
                    tag_name, tag_attribute = t.name, t.attrs #else set whole attribute as tag attribute

                #print(tag_name, tag_attribute)
                
                for element in soup.find_all(tag_name,attrs=tag_attribute):
                    #Clean data
                    text = element.text.strip()
                    text = text.replace('\n', '').replace('\t','')
                    text = re.sub("[\(\[].*?[\)\]]", "", text)
                    text = re.sub(' +', ' ', text)
                    
                    #print(text)
                    try:
                        text = text.split(",")[0]
                    except:
                        pass
                    
                    text = text.split(" ",1)
                    if len(text[1].split(" ")) > 3:
                        break
                    
                    text[0], text[1] = ''.join(e for e in text[0] if e.isalnum()), ''.join(e for e in text[1] if e.isalnum())
                    
                    #If len of text is 2 then save first chunk as firstname and second as lastname
                    if len(text) == 2:
                        First_Name, Last_Name=text[0], text[1]
                        response["data"]["First_Name"].append(First_Name)
                        response["data"]["Last_Name"].append(Last_Name)
                        response["Status"]="Success"
                        self.metadata_dict["Metadata"]={ "Name_Tag":tag_name,
                                                        "Name_Attribute":tag_attribute}
                    
                    #If only first name was found then save that in firstname and save NA in lastname    
                    
                    elif len(text) == 1:
                        First_Name, Last_Name=text[0], "NA"
                        response["data"]["First_Name"].append(First_Name)
                        response["data"]["Last_Name"].append(Last_Name)
                        response["Status"]="Success"
                        self.metadata_dict["Metadata"]={ "Name_Tag":tag_name,
                                                        "Name_Attribute":tag_attribute}
                if response["Status"] == "Success":
                    selected_t = t
                    break        
                
            return response
        except:
            return response #return response

    def get_positions(self, soup):
        try:
            self.tracker = "inside get_positions"
            if self.debug:
                print(self.tracker)
            position_keywords=["Chief executive officer",
                        "Chief operating officer",
                        "Chief financial officer",
                        "Chief information officer",
                        "Chief innovation officer",
                        "Chief strategy officer",
                        "Chief compliance officer",
                        "Chief marketing officer",
                        "Chief talent officer",
                        "Chief human resources officer",
                        "Chief user experience officer",
                        "President",
                        "Partner",
                        "Chairman",
                        "Superintendent",
                        "Chief Solutions Officer"
                        "CEO"]
            possible_tags=["span","div","p","h1","h2","h3","h4","h5","h6","a","pre"]

            self.tracker = "position data loaded"
            if self.debug:
                print(self.tracker)

            for tag in possible_tags:
                for position in position_keywords:
                    possible_format = [
                        position.title(),
                        position.capitalize(),
                        position.upper(),
                        position.lower
                    ]
                    for pos in possible_format:
                        possible_scenario=[
                        f"{pos}",
                        f" {pos}",
                        f"{pos} ",
                        f" {pos} "
                                ]
                        for p in possible_scenario:
                            tag_match = soup.find(tag, text=p)
                            if tag_match!=None:
                                break
                        if tag_match!=None:
                            break
                    if tag_match!=None:
                            break
                if tag_match!=None:
                            break
            
            self.tracker = "Outside for loop: "
            if self.debug:
                print(self.tracker, tag_match)
            #print(tag_match)
            if tag_match!= None:
                self.tracker = "Inside IF "
                if self.debug:
                    print(self.tracker, tag_match)
                position_data = []
                for element in soup.findAll(tag_match.name, attrs=tag_match.attrs):
                    position_data.append(element.text)
                    position_keywords.append(element.text)
                
                self.metadata_dict["Metadata"]["Position_Tag"], self.metadata_dict["Metadata"]["Position_Attribute"]=tag_match.name, tag_match.attrs
                position_keywords = list(dict.fromkeys(position_keywords))
                np.save("./data/position_data.npy", position_keywords)
                return position_data
            else:
                self.tracker = "Inside Else"
                if self.debug:
                    print(self.tracker, tag_match)
                self.metadata_dict["Metadata"]["Position_Tag"], self.metadata_dict["Metadata"]["Position_Attribute"] = "NA", "NA"
                return None
        except Exception as e:
            self.metadata_dict["Metadata"]["Position_Tag"], self.metadata_dict["Metadata"]["Position_Attribute"] = "NA", "NA"
            if self.debug:
                print(e)
            
        
    def gender_API(self, First_Name, Last_Name="NA"):
        if Last_Name=="NA":
            gender = requests.get(f"https://gender-api.com/get?key=vuHPmTBD4Ysd4BZ2AFEcr63kQ6meBASqqEsj&name={First_Name}")
            gender = json.loads(gender.text)
            return gender["gender"]
        else:
            gender = requests.get(f"https://gender-api.com/get?key=vuHPmTBD4Ysd4BZ2AFEcr63kQ6meBASqqEsj&split={First_Name}%20{Last_Name}")
            gender = json.loads(gender.text)
            return gender["gender"]
        
    def web_scrapper(self):
        #print("Web scrapper called")
        #Requesting page data
        #print(URL)
        self.tracker = "Started web scrapping"
        if self.debug:
            print(self.tracker)
        
        headers = {'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36'}
        r = requests.get(self.URL, headers=headers) #request method get html data from that url
        html_doc = r.text #get's text i.e. requests get data as JSON/XML data in which text contains html tree 
        soup = BeautifulSoup(html_doc, features="html5lib") #Converting HTML tree to soup
        self.tracker = "Extracted html"
        #Checks if debugging flag is set to true and prints tracket to detect which part of code have been executed
        if self.debug:
            print(self.tracker)
        #print(soup)
        #Getting Meta data like name and url along with what time it was fetched.
        Company_Name = [n for n in self.URL.split("/")  if "." in n]
        Company_Name = Company_Name[0].split(".")
        Company_Name = Company_Name[1] if len(Company_Name)>2 else Company_Name[0]
        Company_Name = Company_Name.capitalize()
        
        self.metadata_dict["Company_Name"]=Company_Name #Storing company name
        self.metadata_dict["Data_Last_Updated"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S") #getting time when the data was fetched
        
        self.company_html_dict["Company_Name"] = Company_Name
        self.company_html_dict["URL"] = self.URL
        self.company_html_dict["HTML_Data"]=html_doc
        
        text_data = []
        #Finding all text data
        for element in soup.find_all("div"):
            text_data.append(element.text)

        #Removing duplicates if any
        text_data = list(dict.fromkeys(text_data))
            
        self.tracker = "Extracted all div text"    
        if self.debug:
            print(self.tracker)
            
        name_list =[]
        #Getting all the names from text and storing it in name list
        for text in text_data:
            result = self.sentence_creater(text)
            if type(result) == list and len(result)>0:
                for r in result:
                    #print(r)
                    name_list.append(r)
            if type(result) != list:
                if result["Status"] == "Success":
                    #print(result)
                    name_list.append(result)
        
        self.tracker = "Extracted Named Entities using ML"
        if self.debug:
            print(self.tracker)

        try:
            spacy_identified_names = pd.DataFrame(name_list) #Convert it to dataframe
            spacy_identified_names = spacy_identified_names.drop(columns=["Status"]).drop_duplicates().reset_index(drop=True)
        except:
            print("No Name identified. Reason: It is a wrong URL or the website does not allow web scraping")
            response = {"Status":"Fail", "Message": "No Name identified. Reason: It is a wrong URL or the website does not allow web scraping"}
            return
                
            
        if len(name_list)==0:
            return {"Status":"Fail"}

        else:
            for name in name_list:
                response = self.reverse_search_revised(name["First_Name"], soup) #Trying to find tag that contains name using rever search functon
                #print(response)
                if response["Status"] == "Success":
                    name_dict = response["data"]
                    if len(name_dict["First_Name"])>5:
                        break
                else:
                    name_dict = {}

            #If it is successful then use name list provided by reverse search function
            self.tracker = "Extracted named entities using reverse search"
            if self.debug:
                print(self.tracker)

            #If not then return name identified by Spacy
            if name_dict == {}:
                self.tracker = "Could not identify name tag, returning spacy's name"
                if self.debug:
                    print(self.tracker)
                name_df = spacy_identified_names
            
        
        
            try:
                pos_data = self.get_positions(soup)  #Trying to get positions of people
                self.tracker = "Got position data"
                if self.debug:
                    print(self.tracker)
                    
                if pos_data!=None:
                    name_dict["Position"] = pos_data
                name_df = pd.DataFrame(name_dict) #Create dataframe with positions if found
                
                self.tracker = "Data Frame Created"
                if self.debug:
                    print(self.tracker)
                    
            except Exception as e:
                self.tracker = "Inside exception"
                if self.debug:
                    print(self.tracker)
                name_dict.pop("Position",None)
                if "same length" in str(e):
                    name_df = pd.DataFrame(name_dict) #Create dataframe without positon
                else:
                    print(e)
            
            self.tracker = "Past position block"
            if self.debug:
                print(self.tracker)        
            
            name_df=name_df.drop_duplicates() #Droping duplicates
            

            if len(name_df) >= 5:        
                self.data_dict["Company_Name"]=Company_Name
                #If accuracy is false then use gender guesser library in python
                if self.accuracy==False:
                    d = gender.Detector()
                    name_df["Gender"] = name_df["First_Name"].apply(lambda x: d.get_gender(x))
                #Else use Gender-API to identify gender
                else:
                    if "Last_Name" not in name_df.columns:
                        name_df["Gender"] = name_df["First_Name"].apply(lambda x: self.gender_API(x))
                    else:
                        name_df["Gender"] = name_df.apply(lambda x: self.gender_API(x.First_Name, x.Last_Name), axis=1)
                
                n_male = len(name_df.loc[name_df["Gender"]=="male"]) #Get number of males
                n_female = len(name_df.loc[name_df["Gender"]=="female"]) #Get number of females
                diversity_score = (min([n_male, n_female])/max([n_male, n_female])) * 10 #Finding diversity index
                diversity_score = "{:.2f}".format(diversity_score) #Converting it to 2 decimal points
                self.data_dict["Diversity_Index"] = diversity_score #storing diversity score in response
                
                self.data_dict["data"] = name_df.to_dict("records") #Converting dataframe to dictionaries
                response={
                    "Status":"Success",
                    "Metadata_Dict":self.metadata_dict,
                    "Data_Dict":self.data_dict,
                    "HTML_DICT":self.company_html_dict
                }
                self.tracker = "Created response successfully"
                if self.debug:
                    print(self.tracker)
                
                return response #Returning response
            else:
                self.tracker = "Returning fail status"
                if self.debug:
                    print(self.tracker)
                response = {"Status":"Fail"}
                return response #If unsuccessful then returning fail response