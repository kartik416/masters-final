import spacy
import requests
from bs4 import BeautifulSoup
import numpy as np

def web_crawler(URL:str):
    nlp = spacy.load("en_core_web_lg") #Loading spacy model
    #defining common terms used for leadership page
    terminology_list = ["team page", "executive team", "leadership page", "about us", "about", "board of directors", "founders", "leadership", "management-profiles","Management Team","Corporate Governance"]
    #Requesting page data fromgiven URL
    headers = {'User-Agent': 'Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36'}
    r = requests.get(URL, headers=headers) #request method get html data from that url
    html_doc = r.text #get's text i.e. requests get data as JSON/XML data in which text contains html tree 
    
    soup = BeautifulSoup(html_doc, features="html5lib") #Converting HTML tree to soup
    list_of_link = []
    #Finding all anchor tags and storing href attribute data in list
    for element in soup.find_all("a"):
        list_of_link.append(element["href"])
        

    result = []
    #Finding link which have most similiarity with terms I have defined and store them in result array
    for link in list_of_link:
        for terms in terminology_list:
            for l in link.split("/"):
                term = nlp(terms)
                doc = nlp(l)
                similarity_score = term.similarity(doc)
                result.append([terms, link, similarity_score]) 
                
    result = np.array(result)
    indexes = np.where(result[:,2] == max(result[:,2])) #Finding elements which have maximum similarity
    response = []
    for index in indexes[0]:
        #Checking whether it is a full  link or just a route
        if ("www" in result[index][1]) or ("http" in result[index][1]):
            response.append(f"{result[index][1]}")
        else:
            response.append(str(URL)+str(result[index][1])) #If it is just a route then add URL before that
    response = list(dict.fromkeys(response))
    return response #returning list of URLs
    
    