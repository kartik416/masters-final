import pymongo
connection_string = "mongodb://madara416:v01BvdQgeEBeUu5k@cluster0-shard-00-00.qmw78.mongodb.net:27017,cluster0-shard-00-01.qmw78.mongodb.net:27017,cluster0-shard-00-02.qmw78.mongodb.net:27017/Final_Project?ssl=true&replicaSet=atlas-11i81n-shard-0&authSource=admin&retryWrites=true&w=majority"

#Function to put single document
def put_data(db_name:str, collection_name:str ,data: dict):
    client = pymongo.MongoClient(connection_string)
    db = client[db_name]
    collection = db[collection_name]
    collection.insert(data)
    client.close()

#Function to store multiple documents
def export_to_mongo(db_name:str, collection_name:str, insert_data: dict):
    client = pymongo.MongoClient(connection_string)
    db = client[db_name]
    collection = db[collection_name]
    try:
        collection.insert_many(insert_data)
    except Exception as e:
        print(str(e))
    client.close()

#Getting data from mongoDB       
def import_from_mongo(db_name:str, collection_name:str, Base_URL:str):
    client = pymongo.MongoClient(connection_string)
    db = client[db_name]
    collection = db[collection_name]
    data = list(collection.find({"Company_Base_URL":Base_URL}))
    client.close()
    return data

#Function to delete single document
def delete_document(db_name:str, collection_name:str, Company_Name:str):
    client = pymongo.MongoClient(connection_string)
    db = client[db_name]
    collection = db[collection_name]
    query = {"Company_Name":Company_Name}
    collection.delete_one(query)
    client.close()

#Function to delete multiple documents    
def delete_many(db_name:str, collection_name:str, Company_Name:str):
    client = pymongo.MongoClient(connection_string)
    db = client[db_name]
    collection = db[collection_name]
    query = {"Company_Name":Company_Name}
    collection.delete_many(query)
    client.close()

    
    