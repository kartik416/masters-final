from backend.web_crawler import web_crawler
from backend.web_scraping import Web_Scrapper
from tqdm import tqdm
from backend.mongo_store import *


def main(URL, Web_Craweler_Checkbox, Location="NA", Sector="NA"):
    result = import_from_mongo("Final_Project","Company_Metadata",URL) #checking if data exist in result

    #Creating data structure
    Company_Gender_Data={
            "Company_Name":"",
            "Location":Location,
            "Sector":Sector,
            "Diversity_Index":"",
            "Data":[]
            }
    Company_Metadata={    
            "Company_Name":"",
            "Company_Base_URL":"",
            "Company_team_URLs":[],
            "Last_Updated":"",
            "Metadata":{
                "Name_Tag":[],
                "Name_Attribute":[],
                "Position_Tag":[],
                "Position_Attribute":[]
                }
            }

                    
    Company_HTML_Data=[]
            
    Data_Found = False
    
    ans="Y"
    if len(result)>0:
        if ans=="Y":
            #If data already exist in mongoDB then delete it
            Company_Name = result[0]["Company_Name"]
            delete_document("Final_Project","Company_Metadata", Company_Name)
            delete_document("Final_Project","Company_Gender_Data", Company_Name)
            delete_many("Final_Project","Company_HTML_Data", Company_Name)

            
    if ans=="Y":
        try:
            response = []
            #Check if web crawling is selected
            if Web_Craweler_Checkbox==True:
                URLs = web_crawler(URL)#call web scrapper
                Company_Metadata["Company_team_URLs"] = URLs
                for URL in tqdm(URLs):
                    result = Web_Scrapper(URL)#Scrape data from all the result in web craler
                    response.append(result.web_scrapper())#store results
            else:
                Company_Metadata["Company_team_URLs"] = URL
                result = Web_Scrapper(URL) #Scrape data for given URL
                response.append(result.web_scrapper())#append result
            
            #Store returned data in appropriate predefined data structure
            for res in response:
                if res["Status"] == "Success":
                    Data_Found = True
                    #Combining Gender Data
                    Company_Gender_Data["Company_Name"] = res["Data_Dict"]["Company_Name"]
                    Company_Gender_Data["Data"] += res["Data_Dict"]["data"]
                    Company_Gender_Data["Diversity_Index"] = res["Data_Dict"]["Diversity_Index"]
                    
                    #Combining Meta Data
                    Company_Metadata["Company_Name"] = res["Metadata_Dict"]["Company_Name"]
                    Company_Metadata["Company_Base_URL"] = URL
                    Company_Metadata["Last_Updated"] = res["Metadata_Dict"]["Data_Last_Updated"]
                    Company_Metadata["Metadata"]["Name_Tag"].append(res["Metadata_Dict"]["Metadata"]["Name_Tag"])
                    Company_Metadata["Metadata"]["Name_Attribute"].append(res["Metadata_Dict"]["Metadata"]["Name_Attribute"])
                    Company_Metadata["Metadata"]["Position_Tag"].append(res["Metadata_Dict"]["Metadata"]["Position_Tag"])
                    Company_Metadata["Metadata"]["Position_Attribute"].append(res["Metadata_Dict"]["Metadata"]["Position_Attribute"])
                    
                    #Combining Company HTML Data
                    Company_HTML_Data.append(res["HTML_DICT"])
            if Data_Found:
                #If Success then store data in mongodb
                put_data("Final_Project","Company_Gender_Data",Company_Gender_Data)
                put_data("Final_Project","Company_Metadata",Company_Metadata)
                export_to_mongo("Final_Project","Company_HTML_Data",Company_HTML_Data)
                response = {"Status":"Success", "Message":"Data Inserted successfully", "URL":URL}
                print(response)
                return response #return response
            else:
                response = {"Status":"Fail", "Message":"Data Cannot be Scraped", "URL":URL}
                #print(f"{URL} data not found")
                return response
        except Exception as e:
            response = {"Status":"Exception", "Message":str(e), "URL":URL}
            #print(response)
            return response
                
