from website import celery
from website.factory import create_app
from website.celery_queue import init_celery
app = create_app()
init_celery(celery, app) #Initializing celery