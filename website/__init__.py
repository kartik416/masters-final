from flask import Flask
from celery import Celery


#Creating Celery and adding rabbitmq broker and mongoDB server to configuration
def make_celery(app_name = __name__):
    CELERY_BACKEND = "mongodb://madara416:v01BvdQgeEBeUu5k@cluster0-shard-00-00.qmw78.mongodb.net:27017,cluster0-shard-00-01.qmw78.mongodb.net:27017,cluster0-shard-00-02.qmw78.mongodb.net:27017/Final_Project?ssl=true&replicaSet=atlas-11i81n-shard-0&authSource=admin&retryWrites=true&w=majority"
    CELERY_MONGODB_BACKEND_SETTINGS = {
        'database': 'Final_Project',
        'taskmeta_collection': 'my_taskmeta_collection',
    }
    CELERY_BROKER_URL = "amqps://fxbuwwef:GHBkBzSAr-jaor2YU3OySO21S7MykQ15@stingray.rmq.cloudamqp.com/fxbuwwef"
    
    celery = Celery(app_name, backend=CELERY_BACKEND,
                    broker=CELERY_BROKER_URL)
    return celery

celery = make_celery() #creatint celery



def create_app():
    app = Flask(__name__) #initializing flask server
    app.config['SECRET_KEY'] = 'shkfahjfhjkahfjk' #adding secret key

    
    from .views import views #importing views file that contains routing
    
    app.register_blueprint(views, url_prefix='/') #Registeting that file as Blueprint
  
    return app 

