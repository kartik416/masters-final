from flask import Flask
import os
from .celery_queue import init_celery
import website
#from website.dashboard import create_dash_app

def create_app(app_name = __name__, **kwargs):
    app = Flask(app_name) #Creating Flask APP
    if kwargs.get("celery"):
        init_celery(kwargs.get("celery"), app) #If celery in arguments then initialize celery.
    from .views import views #Import view and register it as blueprint
    
    app.register_blueprint(views, url_prefix='/')
    
    return app

