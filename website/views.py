#Import required packages
from flask import Blueprint, render_template, request, redirect,url_for, session
import pandas as pd
import datetime
from backend.main import main
import os
from website import celery
from googlesearch import search


file_storage_path="./temp/" #Setting path for temporary file storage

views = Blueprint('views', __name__) #Creating Blueprint object for view

#Creating celery process
@celery.task(name='celery.process')
def process(URL_Column, Web_Craweler_Checkbox, Location_Column, Sector_Column):
    #Checking for column availables in CSV file and sending data accordingly
    if Location_Column!="None" and Sector_Column!="None":
        for i in range(len(URL_Column)):
            response = main(URL_Column[i], Web_Craweler_Checkbox,Location_Column[i], Sector_Column[i])
    elif Location_Column!="None" and Sector_Column=="None":
        for i in range(len(URL_Column)):
            response = main(URL_Column[i], Web_Craweler_Checkbox,Location_Column[i])
    elif Sector_Column!="None" and Location_Column!="None":
        for i in range(len(URL_Column)):
            response = main(URL_Column[i], Web_Craweler_Checkbox,Sector_Column[i])
    elif Location_Column=="None" and Sector_Column=="None":
        for i in range(len(URL_Column)):
            response = main(URL_Column[i], Web_Craweler_Checkbox)

@views.route('/', methods=['GET','POST'])
def home():
    return render_template("home.html", boolean=True) #rendering home page

@views.route('/extract-form', methods=['GET','POST'])
def extract_form():
    return render_template("extract_form.html", boolean=True) #rendering extract form page

@views.route('/scrape', methods=['GET','POST'])
def scrape():
    file = request.files['inputFile']
    #checking for file
    if file:
        #Checking file format
        if file.filename.endswith('.csv') or file.filename.endswith('.CSV'):
            file.save(file_storage_path+file.filename) #Saving file in temp folder
            return redirect(url_for('views.file_processing', data=file.filename)) #shows file processing page
        else:
            return render_template("error.html", message="File format is not supported, Expected CSV file") #display error message if file not supported
    else:
        URL = request.form['URL'] #If received URL instead of URL then get URL
        Location = request.form['Company Location'] #get location form
        Sector = request.form['Sector'] #get sector from form
        try:
            Web_Craweler_Checkbox = request.form["Web_Crawler"] #Check is user have opted for web crawler or not
            if Web_Craweler_Checkbox == "True":
                Web_Craweler_Checkbox = True
        except:
            Web_Craweler_Checkbox = False
        response = main(URL, Web_Craweler_Checkbox, Location, Sector) #get response
        if response["Status"] == "Success": #If response is successful then set that URL as session
            session["URL"] = URL
                
            return render_template("URL_load.html", URL=URL) #Shows success page with button that redirects to visualization
        else:
            return render_template("error.html", message = response["Message"]) #Show error message
    
@views.route('/file_processing/<data>', methods=['GET','POST'])
def file_processing(data):
    df = pd.read_csv(file_storage_path+data) #Load dataframe
    column_names = list(df.columns) #Show Column names
    return render_template("file_load.html", column_list = column_names, data = data) #render column selector page

@views.route('/file_url_processing/extract/<data>', methods=['GET','POST'])
def extract_batch(data):
    #gets data from form regarding column name
    URL_Column = request.form["URL_Select"]
    Location_Column = request.form["Location_Select"]
    Sector_Column = request.form["Sector_Select"]
    try:
        Web_Craweler_Checkbox = request.form["Web_Crawler"] #getting web crawler status
        if Web_Craweler_Checkbox == "True":
            Web_Craweler_Checkbox = True
    except:
        Web_Craweler_Checkbox = False
    df = pd.read_csv(file_storage_path+data) #read uploaded file
    df = df.dropna() #drop nan values
    os.remove(path=f"{file_storage_path}{data}") #Remove file once data is loaded into dataframe
    estimated_time = len(df)*20 #Giving estimated time using 20 second of average time per URL
    delta = datetime.timedelta(seconds=estimated_time)
    now = datetime.datetime.now()
    estimated_time = (now + delta).strftime("%d/%m/%Y, %H:%M:%S") #Shows time by which process would be completed
    data_extracted = False
    if Location_Column!="None" and Sector_Column!="None":
        URL_Column = df.loc[:,URL_Column].tolist()
        Location_Column = df.loc[:,Location_Column].tolist()
        Sector_Column = df.loc[:,Sector_Column].tolist()
    elif Location_Column!="None" and Sector_Column=="None":
        URL_Column = df.loc[:,URL_Column].tolist()
        Location_Column = df.loc[:,Location_Column.tolist()]
    elif Sector_Column!="None" and Location_Column=="None":
        URL_Column = df.loc[:,URL_Column].tolist()
        Sector_Column = df.loc[:,Sector_Column].tolist()
    else:
        URL_Column = df.loc[:,URL_Column].tolist()
     
    #Initiate process of extraction using Celery task
    process.delay(URL_Column, Web_Craweler_Checkbox, Location_Column, Sector_Column)
    return render_template('extract.html', estimated_time=estimated_time, data_extracted=data_extracted) #Shows page with estimated time

@views.route('/file_url_processing/success', methods=["GET", "POST"])
def success_page():
    return render_template('extract.html', estimated_time=None, data_extracted=True)


@views.route('/search',methods=["GET", "POST"])
def search_result():
    #fetching data from search box
    query = request.form["Search_Box"]
    Search_Results = []
    #searching google query result and returning it to page
    for j in search(query, tld="co.in", num=10, stop=10, pause=2):
        Search_Results.append(j)
    return render_template('search.html', Search_Results= Search_Results) #Rendering search results

