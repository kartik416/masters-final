from celery import Celery

def init_celery(celery, app):
    celery.conf.update(app.config) #Updating configuration
    TaskBase = celery.Task #Add Task to TaskBase
    #Create Task for Celery
    class ContextTask(TaskBase):
        abstract=True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self,*args,**kwargs)
    celery.Task = ContextTask #Sets Celery Task