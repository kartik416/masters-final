#Importing required packages
import dash
import dash_html_components as html
import dash_core_components as dcc
from flask import session
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import dash_core_components as dcc
from dashboard_utilities import Utilities

#Creating function that takes flask server instance for dash to run
def create_dash_app(flask_app):
    """Create a Plotly Dash dashboard."""
    external_stylesheets = [dbc.themes.SKETCHY] #Adding stylesheet
    app = dash.Dash(
        server=flask_app,
        url_base_pathname='/dashapp/',
        name="Dashboard",
        external_stylesheets=external_stylesheets
    )#creating dash application

    
    # Create Dash Layout
    URL = "https://www.apple.com/ie/leadership/" #using as dummy data for visualization
    
    utility = Utilities(URL) #Creating dash utility instance
    gender_df, diversity_df = utility.create_dataframes() #Creating dataframe
    Locations, Sector = gender_df["Location"].unique(), gender_df["Sector"].unique() #getting unique values to display in drop down menu
    #print("#################", Locations, Sector)
    try:
        Locations, Sector = filter(None, Locations), filter(None, Sector) #removing none values
    except:
        pass


    #Creating checklist for location and sector filter
    checklist_Location = dbc.FormGroup(
        [
            dbc.Label("Filter by Country"),
            dbc.Checklist(
                options=[
                    {'label': i, 'value': i} for i in Locations
                ],
                value=[1],
                id="checklist-input-location",
            ),
        ]
    )

    checklist_Sector = dbc.FormGroup(
        [
            dbc.Label("Filter by Sector"),
            dbc.Checklist(
                options=[
                    {'label': i, 'value': i} for i in Sector
                ],
                value=[1],
                id="checklist-input-sector"
            ),
        ]
    )

    #Creating app layout
    app.layout = html.Div(
        [   dcc.Input(
          id="hidden",
          value=URL,
          style={"display": "none"}  
        ),
            dbc.Row([
                dbc.Col(dcc.Graph(
                    id="fig_1"),
                    width=3,
                    align = 'center',
                    style={"border":"1px solid black"}
                    ),
                dbc.Col(
                    dcc.Graph(
                    id="fig_2"),
                    width=4,
                    align = 'center',
                    style={"border":"1px solid black"}
                    ),
                dbc.Col(
                    dcc.Graph(
                    id="fig_3"),
                    width=3,
                    align = 'center',
                    style={"border":"1px solid black"}
                    ),
                dbc.Col(
                    checklist_Location,
                    width=2,
                    align = 'right',
                    style={"border":"1px solid black"}
                )
            ],
            className="h-20"),
            dbc.Row([
                dbc.Col(
                dcc.Graph(
                    id="fig_4"),
                    width=5,
                    align = 'center',
                    style={"border":"1px solid black"}
                ),
                dbc.Col(
                dcc.Graph(
                    id="fig_5"),
                    width=5,
                    align = 'center',
                    style={"border":"1px solid black"}
                ),
                dbc.Col(
                    checklist_Sector,
                    width=2,
                    align = 'right',
                    style={"border":"1px solid black"}
                )
                ],
                className="h-70") 
        ],
        style={"width":"100%", "height":"100%", "overflow-x":"hidden"}
    )

    @app.callback(
        [Output("fig_1","figure"),
        Output("fig_2","figure"),
        Output("fig_3","figure"),
        Output("fig_4","figure"),
        Output("fig_5","figure"),
        Output("hidden","value")],
        [Input("checklist-input-location","value"),
        Input("checklist-input-sector","value")]
    )
    def update_graph(Location_Filter, Sector_Filter):
        #print(Location_Filter, Sector_Filter)
        Location_Filter.pop(0), Sector_Filter.pop(0)#removing none data
        if session.get("URL"): 
            URL = session.get("URL") #getting URL from flask session storage
        else:
            URL = "https://www.apple.com/ie/leadership/"
        
        utility = Utilities(URL, Location_Filter, Sector_Filter) #creating new utility instance with filter data
        gender_df, diversity_df = utility.create_dataframes()
        Company_Name = [n for n in URL.split("/")  if "." in n]
        Company_Name = Company_Name[0].split(".")
        Company_Name = Company_Name[1] if len(Company_Name)>2 else Company_Name[0]
        Company_Name = Company_Name.capitalize()

        #print(gender_data["Gender"].value_counts())
        Rank = diversity_df.loc[diversity_df["Company"]==Company_Name, "Rank"].values[0] #finding rank of company 

        fig1 = go.Figure()
        fig1.add_trace(go.Indicator(
            mode = "number+delta",
            value = Rank,
            title = {"text": f"{Company_Name.upper()} RANK<br><span style='font-size:0.8em;color:gray'>In our database</span><br><span style='font-size:0.8em;color:gray'>Amongst {len(diversity_df)} Companies</span>"},
            ))#Rank indicator

        fig2 = go.Figure()
        fig2.add_trace(go.Indicator(
            mode = "number+delta",
            value = float(diversity_df.loc[diversity_df["Company"]==Company_Name, "Diversity_Index"].values[0]),
            title = {"text": f"{Company_Name.upper()} GENDER DIVERSITY <br> INDEX<br><span style='font-size:0.8em;color:gray'>Based on Gender Ratio</span><br><span style='font-size:0.8em;color:gray'>Amongst {len(diversity_df)} Companies</span>"},
            delta = {'reference': float(max(diversity_df["Diversity_Index"])), 'relative': True})) #Diversity Index indicator

        labels = gender_df["Gender"].unique()
        values = gender_df["Gender"].value_counts()

        fig3 = go.Figure(data=[go.Pie(labels=labels, values=values, hole=0.7)]) #Creating doughnut chart
        fig3.update_layout(title="Overall Gender Percentage")

        fig4 = utility.create_bar_graph("Company") #Getting chart from utility class for comparison of top 10 companies

        fig5 = utility.create_bar_graph("Sector") #Getting chart from utility class for avg gender in each sector
        
        return [fig1, fig3, fig2, fig4, fig5, URL] #returning figures to dash

    return app #Returning Final App