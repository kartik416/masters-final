#Importing all relevant module which contains creation code for Flask, Celery and Dash Application
from website import factory
import website
from website.dashboard import create_dash_app

#If name of server is main the execute these statements
if __name__=='__main__':
    app = factory.create_app(celery=website.celery) #Creates Flask server along with Celery instance that takes Flask server as input.
    app.secret_key = "Drmhze6EPcv0fN_81Bj-nA" #Any random key to enable session storage
    create_dash_app(app) #Creating Dash app which connects to existing Flask instance
    app.run(host='0.0.0.0',debug=False, port='8080') #Running app
