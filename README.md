# Masters-Final
In this repository you have requirements.txt file which contains list of packages that is required to run this project.
main.py file is entry point to my application which intiates flask server and run the application on the local host.

Backend folder contains all the backend functionality code i.e. Web crawler, Web Scraping, mongo_store.py which contains basic function to
perform CRUD operations and a main.py file which integrates all the function.

Temp folder temporarily store file fetched from th Flask. Once the data is processed the file inside temp folder is deleted.

Website folder contains my frontend and middleware code like celery queue setup, dashboard, HTML templates and Routes. 

Steps to setup this Project:
1) Install all dependecies using command python3 install -r requirements.txt
2) Additionally you need to install Spacy according to configuration you need. You can see options to install SpaCy on https://spacy.io/usage
3) After successfully installation of spacy run these commands python3 -m spacy download en_core_web_trf, python3 -m spacy download en_core_web_lg
4) To run the application use command python3 main.py
5) In another terminal/commandline use command celery -A celery_worker.celery worker --loglevel=info
