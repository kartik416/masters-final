#Importing all relevant modules
import pandas as pd
import plotly.express as px
import pymongo
import numpy as np

#Creating a class.
class Utilities:
    #Creating a init function that initializes all the parameters whenever a instance of this class is created. It expect URL as needed argument and Location_Filter and Sector_Filter as optional argument.
    def __init__(self, URL:str, Location_Filter:list = [], Sector_Filter:list = []):
        self.URL = URL #storing URL in self.URL so that all the methods in this class can access the data/variable.
        #The process below helps to create company name from URL
        Company_Name = [n for n in self.URL.split("/")  if "." in n]
        Company_Name = Company_Name[0].split(".")
        Company_Name = Company_Name[1] if len(Company_Name)>2 else Company_Name[0]
        Company_Name = Company_Name.capitalize()
        
        #Setting variables that should be accessible by all methods
        self.Location_Filter = Location_Filter
        self.Sector_Filter = Sector_Filter
        self.Company_Name = Company_Name
        
        #Connection string for MongoDB connection
        self.Connection_String = "mongodb://madara416:v01BvdQgeEBeUu5k@cluster0-shard-00-00.qmw78.mongodb.net:27017,cluster0-shard-00-01.qmw78.mongodb.net:27017,cluster0-shard-00-02.qmw78.mongodb.net:27017/Final_Project?ssl=true&replicaSet=atlas-11i81n-shard-0&authSource=admin&retryWrites=true&w=majority"
        
    def create_dataframes(self):
        client = pymongo.MongoClient(self.Connection_String) #Creating Mongo Client
        db = client["Final_Project"] #Connecting to database Final_Project       
        collection = db['Company_Gender_Data'] # Loading the collection Company_Gender_Data from database
        results = []
        
        #Filtering data according to the parameters on database level and storing result of query in result list.
        if self.Location_Filter == [] and self.Sector_Filter == []:
            for doc in collection.find():
                results.append(doc)
                
        elif self.Location_Filter != [] and self.Sector_Filter != []:
            for doc in collection.find({"$or":[{"Location":{"$in":self.Location_Filter}, "Sector": {"$in":self.Sector_Filter}}, {"Company_Name":self.Company_Name}]}):
                results.append(doc)
                
        elif self.Location_Filter != [] and self.Sector_Filter == []:
            for doc in collection.find({"$or":[{"Location":{"$in":self.Location_Filter}}, {"Company_Name":self.Company_Name}]}):
                results.append(doc)
                
        elif self.Location_Filter == [] and self.Sector_Filter != []:
            for doc in collection.find({"$or":[{"Sector": {"$in":self.Sector_Filter}}, {"Company_Name":self.Company_Name}]}):
                results.append(doc)
                
        client.close() #Closing the connection with MongoDB server.
        
        #Creating dataframe from the data that was fetched
        flag = True
        for r in results:
            if flag:
                diversity_df = pd.DataFrame(data=np.array([r['Company_Name'] ,r['Location'], r['Sector'], r['Diversity_Index']]).reshape((1,4)), columns=["Company", "Location", "Sector", "Diversity_Index"])
                gender_df = pd.DataFrame(r['Data'])
                gender_df["Company"], gender_df["Location"], gender_df["Sector"] = r['Company_Name'] ,r['Location'], r['Sector']
                flag=False
            else:
                temp_df = pd.DataFrame(r['Data'])
                temp_df["Company"], temp_df["Location"], temp_df["Sector"] = r['Company_Name'] ,r['Location'], r['Sector']
                gender_df = pd.concat((gender_df, temp_df))
                temp_df = pd.DataFrame(data=np.array([r['Company_Name'] ,r['Location'], r['Sector'], r['Diversity_Index']]).reshape((1,4)), columns=["Company", "Location", "Sector", "Diversity_Index"])
                diversity_df = pd.concat((diversity_df, temp_df))
            
        #diversity_df contains data like company name, location, sector and diversity index
        #gender_df contains company name, location, sector, first name, last name, gender    
            
        gender_df = gender_df.reset_index(drop = True) #Resetting Indexes for gender dataframe
        diversity_df = diversity_df.reset_index(drop = True) #Resetting Indexes for diversity dataframe
        diversity_df["Diversity_Index"] = diversity_df["Diversity_Index"].apply(lambda x: "{:.2f}".format(float(x)))  #Coverting GDI to 2 decimal point
        diversity_df = diversity_df.sort_values(by="Diversity_Index", ascending=False, ignore_index=True) #Sorting the dataframe based on diversity index in descending order.
        diversity_df.insert(0, "Rank", range(1,len(diversity_df)+1)) #Inserting Rank column 
        
        return gender_df, diversity_df #Returning Final Df
    
    #This function creates bar graph which is used in dashboard
    def create_bar_graph(self, By:str):
        By = By.lower().capitalize()      
        if  By == "Company":
            gender_df, diversity_df = self.create_dataframes() #Creating dataframe
            merge_df = gender_df.merge(diversity_df[["Company","Rank"]], on="Company") #Merging both the dataframe
            Rank = diversity_df.loc[diversity_df["Company"]==self.Company_Name, "Rank"].values[0] #Finding data for given company name
            # The code block below fetches top 10 companies along with the company name that is being analyzed.
            try:
                if Rank <= 10:
                    top_10 = diversity_df.loc[:9,"Company"].tolist()
                else:
                    top_10 = diversity_df.loc[:8,"Company"].tolist()
                    top_10.append(self.Company_Name)
            except:
                top_10 = diversity_df.loc[:,"Company"]
            
                
            top_10_df = merge_df.loc[merge_df.apply(lambda x: x.Company in top_10, axis=1),:] #Getting all data related to top 10 companies
            top_10_df = top_10_df[["Rank","Company","Gender"]] #Getting required columng i.e., Rank, Company Name, Gender data.
            top_10_df = top_10_df.sort_values(by=["Rank"]) #Sorting dataframe by rank
            
            top_10_df["Company"] = top_10_df.apply(lambda x: x.Company if x.Company==self.Company_Name else f"Company {x.Rank}", axis=1) #Using a lambda function to mask other company names
            
            top_10_df = top_10_df.groupby(["Company","Gender"], sort="False").size().reset_index() #Using group by method to find count of gender in each company
            top_10_df = top_10_df.pivot(index="Company", columns="Gender").reset_index() #Pivoting table with company as index and Gender as column
            top_10_df = top_10_df.reset_index(drop=True).set_axis(["Company","Female","Male","Unknown"], axis=1) #Converting to dataframe which have column Company name, Female count, Male Count and Unknown.
            top_10_df["Male_Percent"] = top_10_df["Male"]*100/(top_10_df["Male"]+top_10_df["Female"]) #Calculating Percentage for Male
            top_10_df["Female_Percent"] = top_10_df["Female"]*100/(top_10_df["Male"]+top_10_df["Female"])  #Caluclating Percentrage for Female
            
            
            top_10_df = pd.melt(top_10_df, id_vars=["Company"], value_vars=["Male_Percent","Female_Percent"], var_name="Type") #Melting it to reduce columns      
            #Creating horizontal stacked bar chart.
            Company_Comparison_Graph = px.bar(top_10_df, x="value", y="Company", color="Type", orientation='h' , title="Comparison With Top Companies in our Database", labels={"Company":"Company Name", "value":"Percentage"})
            return Company_Comparison_Graph
        
        if By == "Sector":
            gender_df, diversity_df = self.create_dataframes() #Getting Dataframe
            Sector_df = gender_df.groupby(["Sector","Company","Gender"], sort=False).size().reset_index() #Finding nuber of male and female in each company
            Sector_df = Sector_df.groupby(["Sector","Gender"]).agg(['mean']) #Getting average of gender accross various sector.
            Sector_df = Sector_df.reset_index().set_axis(["Sector","Gender","Average_Number"], axis=1) #Converting it to dataframe.
            #Creating Group Bar Chart
            Sector_Average_Graph = px.bar(Sector_df, x="Sector", y="Average_Number", color="Gender", barmode="group", title="Sector wise average gender count according to our Database", labels={"Average_Number":"Avearge Number","Sector":"Sectors"})
            return Sector_Average_Graph

        
            